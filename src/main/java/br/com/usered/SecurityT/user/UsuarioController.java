package br.com.usered.SecurityT.user;


import br.com.usered.SecurityT.user.dtos.UsuarioDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public UsuarioDTO cadastrarUsuario(@RequestBody Usuario usuario){
       Usuario objUsuario = usuarioService.cadastrarUsuario(usuario);
        return modelMapper.map(objUsuario, UsuarioDTO.class);
    }

    @GetMapping("/{idUsuario}")
    public  UsuarioDTO pesquisarUsuarioPeloId(@PathVariable(name ="idUsuario") int id){
        try {
            Usuario objUsuario = usuarioService.buscarUsuarioPeloId(id);
            return  modelMapper.map(objUsuario, UsuarioDTO.class);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
