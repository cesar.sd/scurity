package br.com.usered.SecurityT.postagem;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostagemRepository extends CrudRepository<Postagem, Integer> {

    List<Postagem> findBAllyUsuarioId(int usuarioId);
}
