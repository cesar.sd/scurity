package br.com.usered.SecurityT.postagem;


import br.com.usered.SecurityT.user.Usuario;
import br.com.usered.SecurityT.user.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PostagemService {
    @Autowired
    private PostagemRepository postagemRepository;
    @Autowired
    public UsuarioService usuarioService;

    public Postagem cadastrarPostagem(int idUsuario, Postagem postagem) {
        Usuario usuario = usuarioService.buscarUsuarioPeloId(idUsuario);

        postagem.setUsuario(usuario);
        postagem.setData(LocalDate.now());

        return postagemRepository.save(postagem);
    }

    public Postagem CadastrarPostagem(String email, Postagem postagem) {
        Usuario usuario = usuarioService.buscarUsuarioPeloEmail(email);

        postagem.setUsuario(usuario);
        postagem.setData(LocalDate.now());

        return postagemRepository.save(postagem);
    }

    public List<Postagem> pesquisarPostagemPeloId(int idUsuario) {
        return postagemRepository.findBAllyUsuarioId(idUsuario);
    }

    public Boolean mensagemDoAutor(int idMensagem, String emailAuthor) {
        Optional<Postagem> postagemOptional = postagemRepository.findById(idMensagem);

        postagemOptional.orElseThrow(() -> new RuntimeException("Postagem não encontrada"));

        if (!postagemOptional.get().getUsuario().getEmail().equals(emailAuthor)) {
            return false;
        }
        return true;
    }
}



